# api-rest-laravel

API-REST construida por medio del framework Laravel

## Conceptos a desarrollar

API-REST, API pública, Protocolo HTTP

## Tecnologías

Lenguaje: Php

Framework: Laravel

Gestor de paquetes de PHP: Composer

CLI: Artisan (stá incluida en Laravel)

Gestor de Base de Datos: MySQL

Prueba de la API: Postman (Opcional)

## Uso
Para levantar el servidor ejecutar el siguiente comando:

```bash
1) php artisan serve
```

## URIs
GET --> /contacts

GET --> /contacts/:id

POST --> /contacts

DELETE --> /contacts/:id

PUT --> /contacts/:id

## Tipos de contenido del retorno

application/json

## Cliente

El siguiente proyecto tiene habilitado por medio de un middleware el acceso a la API

[client-angular](https://gitlab.com/projects-angular-7/client-angular)

## Referencias
[EDteam](https://www.youtube.com/watch?v=fclXCIc34qM&utm_source=sendinblue&utm_campaign=EDtaller_180_Agradecimiento__Crea_tu_primera_API_con_Laravel&utm_medium=email)

[datadriveninvestor](https://medium.com/datadriveninvestor/how-to-build-a-rest-api-in-laravel-6-0-71a1487fb406)

[CORS](https://laravel.com/docs/5.8/middleware)

[CORS Conflicto - Solución Diego Machado](https://medium.com/p/2b574c51d0c1/responses/show)
